package com.lxf.spring.pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author lixiaofei
 * @create 2021/11/12 - 11:18 上午
 */

public class User {

    private Integer id;
    private String name;
    private String sex;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", sex='" + sex + '\'' +
            ", age=" + age +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        LinkedList<Object> objects1 = new LinkedList<>();
        objects1.add("1");

        Properties properties = new Properties();


        LinkedHashSet<Object> objects = new LinkedHashSet<>();
        objects.add("1");
        objects.contains("");

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(name, user.name) && Objects
            .equals(sex, user.sex) && Objects.equals(age, user.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, sex, age);
    }


    public static class ThreadTask extends Thread{
        @Override
        public void run() {
            System.out.println("Thread线程初始化");
        }
    }
    public static class RunnableTask implements Runnable{
        @Override
        public void run() {
            System.out.println("Runnable线程初始化");
        }
    }
    public static class CallableTask implements Callable<String> {
        @Override
        public String call() throws Exception {
            System.out.println("Callable<String>线程初始化");
            return "Callable<String>线程初始化返回值";
        }
    }

    public static void main(String[] args) throws Exception{
        new ThreadTask().start();
        new Thread(new RunnableTask()).start();
        FutureTask<String> futureTask = new FutureTask<>(new CallableTask());
        new Thread(futureTask).start();
        System.out.println(futureTask.get());



    }


}
