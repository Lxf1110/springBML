package com.lxf.spring.dao;


import com.lxf.spring.pojo.User;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author lixiaofei
 * @create 2021/11/12 - 11:18 上午
 */
@Mapper //为了让spring扫描到bean。也可以在启动类加注解扫描。二者用其一即可、不可重复扫描
@Repository
public interface UserDao {

    /**
     * 获取所有用户记录
     * @return
     */
    User getUserById(Integer id);
    List<Map> getUserLikeName(String Name);
    List<Map> getUserByMap(HashMap map);

}
