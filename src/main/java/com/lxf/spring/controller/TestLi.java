package com.lxf.spring.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author lixiaofei
 * @create 2022/3/14 - 4:00 下午
 */
public class TestLi {







    public static void main(String[] args) {
        long scope=100;
        //求0-scope范围内质数
        ArrayList prime = prime(scope);
        //创建随机排序数组
        Collections.shuffle(prime);
        //随机100个质数
        List<Long> objects = prime.subList(0, 100);
        //排序
        Collections.sort(objects);
        System.out.println(objects);
    }

    /**
     * 获取一定范围内的所有质数
     * @param scope
     * @return
     */
    private static ArrayList prime(long scope) {
        ArrayList<Object> primes = new ArrayList<>();
        for(int i=2;i<=scope;i++) {
            for(int j=2;j<=i;j++) {
                if(i%j==0 && i!=j) {
                    break;
                }
                if(j==i) {
                    primes.add(i);
                }
            }
        }
        return primes;
    }


}
