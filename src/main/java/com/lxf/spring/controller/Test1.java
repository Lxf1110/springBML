package com.lxf.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import redis.clients.jedis.Jedis;

/**
 * @author lixiaofei
 * @create 2022/3/14 - 4:43 下午
 */
public class Test1 {

    public static void main(String[] args) {
        // 连接本地的 Redis 服务
//        Jedis jedis = new Jedis("47.102.192.95"); // 默认端口

        Jedis jedis = new Jedis("47.102.192.95",6379); // 指定端口

        // jedis.auth("redis"); // 指定密码
        System.out.println("Connection to server sucessfully");
        // 设置 redis 字符串数据
//        jedis.set("redis", "Redis 1");
        // 获取存储的数据并输出
        System.out.println("Stored string in redis:: " + jedis.get("name"));

        System.out.println("redis : " + jedis.get("age"));

    }

}
