package com.lxf.spring.controller;

import com.lxf.spring.SpringBootMybatisLog4jApplication;
import com.lxf.spring.dao.UserDao;
import com.lxf.spring.pojo.User;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.PropertiesBeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lixiaofei
 * @create 2021/11/12 - 10:37 上午
 */
@Controller
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    UserDao userDao;

    @RequestMapping("/getUserById")
    @ResponseBody
    public String getUserById(){

        User users = userDao.getUserById(1);
        String usersStr = users.toString();
        logger.info(usersStr);
        return usersStr;
    }
    @RequestMapping("/getUserLikeName")
    @ResponseBody
    public String getUserLikeName(){
        List<Map> users = userDao.getUserLikeName("小");
        return users.toString();
    }
    @RequestMapping("/getUserByMap")
    @ResponseBody
    public String getUserByMap(){
        HashMap<Object, Object> map = new HashMap<>();
        map.put("name","小");
        InputStreamSource classPathResource = new ClassPathResource("");

        new ClassPathXmlApplicationContext("");
        map.put("age",17);
        List<Map> userByMap = userDao.getUserByMap(map);
        return userByMap.toString();
    }
}
