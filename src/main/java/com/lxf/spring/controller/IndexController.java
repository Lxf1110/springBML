package com.lxf.spring.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lixiaofei
 * @create 2021/11/26 - 10:52 上午
 */
@Controller
public class IndexController {
    @RequestMapping({"/index", "/"})
    public String index() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("");
        String s="";
        HashSet hashSet = new HashSet();
        hashSet.add("");
        arrayList.add(1);
        return "index";
    }

}
